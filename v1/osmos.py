import pyglet
from game.render.render import game
from game.render.render import WorldMaschine
from game.Cell import Cell
from game.utils import getId


window = game()
worldProperties = WorldMaschine()

gameObjects = [
    Cell(getId(), 50, 50, 50, worldProperties,
         window.width, window.height, True, 0, 0, False),

    Cell(getId(), 470, 470, 10, worldProperties,
         window.width, window.height, False, 1, 2, False),
    Cell(getId(), 270, 870, 30, worldProperties,
         window.width, window.height, False, 7, 3, False),
    Cell(getId(), 400, 410, 13, worldProperties,
         window.width, window.height, False, 8, 2, False),
    Cell(getId(), 770, 70, 75, worldProperties,
         window.width, window.height, False, 3, 4, False)
]


@window.event
def on_draw():
    window.clear()
    for element in gameObjects:
        element.draw()


def check_collisions(cells):
    cells = list(filter(lambda cell: isinstance(cell, Cell), cells))

    for element in cells:
        others = list(filter(lambda cell: cell.id != element.id, cells))
        for cell in others:
            if not element.isBubble and not cell.isBubble:
                if (element.collides_with(cell)):
                    element.bounce(cell)
                element.attract(cell)
            else:
                element.collides_with(cell)


def update(dt):
    global gameObjects

    check_collisions(gameObjects)

    newGameObjects = []
    for element in gameObjects:
        if (element.radius > 1):
            element.update(dt)
            newGameObjects.append(element)
    gameObjects = newGameObjects


@window.event
def on_mouse_press(x, y, button, modifiers):
    global new_direction
    new_direction = (x, y)
    if (gameObjects[0].isPlayer == True):
        gameObjects.append(gameObjects[0].impulse(x, y))


# window.push_handlers(on_key_press)

pyglet.clock.schedule_interval(update, 1/60)

pyglet.app.run()
