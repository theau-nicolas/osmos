def isCellCollide(C1, C2):
    d2 = (C1.x - C2.x) ** 2 + (C1.y - C2.y) ** 2
    if d2 > (C1.rayon + C2.rayon) ** 2:
        return False
    else:
        return True
