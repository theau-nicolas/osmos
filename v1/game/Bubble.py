import pyglet
from math import atan2, cos, sin


class Bubble:
    def __init__(self, id, x, y, speed_x, speed_y):
        self.id = id
        self.x = x
        self.y = y
        self.speed_x = speed_x
        self.speed_y = speed_y
        self.witness = pyglet.shapes.Circle(
            x, y, 4, color=(255, 0, 0))
        self.radius = 4

    def draw(self):
        self.witness.draw()

    def update(self, dt):
        self.x += self.speed_x * dt
        self.y += self.speed_y * dt
        self.witness.position = (self.x, self.y)
        self.witness.draw()

    def collides_with(self, targetCell):
        d2 = (self.x - targetCell.x) ** 2 + (self.y - targetCell.y) ** 2
        if d2 > (self.radius + targetCell.radius) ** 2:
            self.color = (255, 0, 0)
            return False
        else:
            self.color = (0, 255, 0)
            # transfer
            if (targetCell.radius > self.radius):
                targetCell.radius += .1
                self.radius -= .1
            else:
                targetCell.radius -= .1
                self.radius += .1
            return True
