import pyglet
from math import atan2, cos, sin, sqrt
from game.Bubble import Bubble
from game.utils import getId


class Cell:
    def __init__(self, id, x, y, radius, worldProperties, window_width, window_height, isPlayer, iSpeed_x, iSpeed_y, isBubble):
        self.id = id
        self.isPlayer = isPlayer
        self.x = x
        self.y = y
        self.radius = radius
        self.mass = radius*10
        self.witness = pyglet.shapes.Circle(
            x, y, radius, color=(255, 0, 0))
        self.speed = 0
        self.direction = 0
        self.color = (255, 0, 0)
        self.speed_x = iSpeed_x
        self.speed_y = iSpeed_y
        self.worldProperties = worldProperties
        self.dump = worldProperties.dumping
        self.force = 10
        self.window_width = window_width
        self.window_height = window_height
        self.isBubble = isBubble
        self.attractionCoeff = 200

    def update(self, dt):
        if self.speed_x > 0:
            self.speed_x -= self.dump * dt
        else:
            self.speed_x += self.dump * dt

        if self.speed_y > 0:
            self.speed_y -= self.dump * dt
        else:
            self.speed_y += self.dump * dt

        self.x += self.speed_x * dt
        self.y += self.speed_y * dt

        # Vérifier si la bulle est dans la window sinon la faire rebondir
        if self.x - self.radius < 0:
            self.x = self.radius
            self.speed_x *= -1
        elif self.x + self.radius > self.window_width:
            self.x = self.window_width - self.radius
            self.speed_x *= -1
        if self.y - self.radius < 0:
            self.y = self.radius
            self.speed_y *= -1
        elif self.y + self.radius > self.window_height:
            self.y = self.window_height - self.radius
            self.speed_y *= -1

        self.mass = self.radius

        self.witness.radius = self.radius
        self.witness.position = (self.x, self.y)
        self.witness.color = self.color
        self.witness.draw()

    def draw(self):
        self.witness.draw()

    def move(self, dx, dy):
        self.x += dx
        self.y += dy

    def impulse(self, direction_x, direction_y):
        dx = direction_x - self.x
        dy = direction_y - self.y
        angle = atan2(dy, dx)
        self.radius -= 1
        self.speed_x -= self.force * cos(angle)
        self.speed_y -= self.force * sin(angle)
        return (Cell(getId(), self.x, self.y, 2, self.worldProperties, self.window_width, self.window_height, False, (dx*3), (dy*3), True))

    def collides_with(self, targetCell):
        d2 = (self.x - targetCell.x) ** 2 + (self.y - targetCell.y) ** 2
        if d2 > (self.radius + targetCell.radius) ** 2:
            self.color = (255, 0, 0)
            return False
        else:
            self.color = (0, 255, 0)
            # transfer
            if (targetCell.radius > self.radius):
                targetCell.radius += .5
                self.radius -= .5
            else:
                targetCell.radius -= .5
                self.radius += .5
            return True

    # def bounce(self, targetCell):
    #     print("bounce")

    def attract(self, targetCell):
        print("attract")

    def bounce(self, targetCell):
        dx = targetCell.x - self.x
        dy = targetCell.y - self.y
        angle = atan2(dy, dx)

        self.speed_x -= (targetCell.force * cos(angle))*.02
        self.speed_y -= (targetCell.force * sin(angle))*.02

    def attract(self, targetCell):

        d2 = (self.x - targetCell.x) ** 2 + (self.y - targetCell.y) ** 2
        if d2 > ((self.radius*(self.mass / self.attractionCoeff)) + (targetCell.radius * (self.mass / self.attractionCoeff))) ** 2:

            self.color = (255, 0, 0)
            return False
        else:
            dx = targetCell.x - self.x
            dy = targetCell.y - self.y
            angle = atan2(dy, dx)

            targetCell.speed_x -= self.mass / \
                self.attractionCoeff * cos(angle) * 20
            targetCell.speed_y -= self.mass / \
                self.attractionCoeff * sin(angle) * 20

            self.color = (0, 0, 255)
            return True
