import pyglet


def newCell(window):
    x, y = window.width // 2, window.height // 2
    rayon = 50
    return pyglet.shapes.Circle(x, y, rayon, color=(255, 0, 0))
