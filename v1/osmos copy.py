import pyglet

# Créer une fenêtre de jeu
window = pyglet.window.Window()

# Créer un cercle rouge au centre de la fenêtre
x, y = window.width // 2, window.height // 2
rayon = 50
cercle = pyglet.shapes.Circle(x, y, rayon, color=(255, 0, 0))

# Définir la position vers laquelle déplacer le cercle
nouvelle_position = (window.width - rayon, window.height - rayon)

# Définir la durée de l'interpolation
duree = 120.0  # en secondes

# Définir le temps écoulé depuis le début de l'interpolation
temps_ecoule = 0.0  # en secondes


def update(dt):
    global temps_ecoule

    # Mettre à jour le temps écoulé depuis le début de l'interpolation
    temps_ecoule += dt

    # Calculer la progression de l'interpolation
    progression = min(temps_ecoule / duree, 1.0)

    # Calculer la nouvelle position du cercle en utilisant l'interpolation lerp()
    cercle.x, cercle.y = cercle.x + \
        (nouvelle_position[0] - cercle.x) * progression, cercle.y + \
        (nouvelle_position[1] - cercle.y) * progression

    # Arrêter la mise à jour si l'interpolation est terminée
    if progression == 1.0:
        pyglet.clock.unschedule(update)


@window.event
def on_draw():
    window.clear()
    cercle.draw()


@window.event
def on_mouse_press(x, y, button, modifiers):
    global nouvelle_position, temps_ecoule

    # Mettre à jour la position vers laquelle déplacer le cercle
    nouvelle_position = (x, y)

    # Réinitialiser le temps écoulé depuis le début de l'interpolation
    temps_ecoule = 0.0

    # Planifier la mise à jour de la position du cercle à chaque frame
    pyglet.clock.schedule(update)


# Lancer le programme
pyglet.app.run()
